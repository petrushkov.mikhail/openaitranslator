import sys
import csv
import json
import os
from openai import OpenAI


def get_token():
    with open("token.txt", "r") as f:
        s = f.read()
        return " ".join(s.split())


def get_gpt_answer_from_array(messages: list, model='gpt-4-1106-preview'):
    json = False
    for msg in messages:
        if "json" in msg["content"].lower():
            json = True
            break

    try:
        with OpenAI(api_key=get_token()) as client:
            if json:
                completion = client.chat.completions.create(model=model,
                                                            messages=messages,
                                                            response_format={"type": "json_object"}, seed=42)
            else:
                completion = client.chat.completions.create(model=model, messages=messages, seed=42)
        return completion.choices[0].message.content
    except Exception as ex:
        print(f"\n!!!! AI Exception: {ex}\n")
        return None


def get_intro():
    with open("prompt.txt", "r") as f:
        return f.read()


def get_source(filename):
    result = []
    with open(filename, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        header = None
        index = 0
        for line in reader:
            index += 1
            if header is None:
                header = line
                result.append({})
                continue

            if line[0].strip() == "":
                result.append({})
                continue

            item = {'id': line[0]}
            for i in range(1, len(line)):
                item[header[i]] = line[i]

            result.append(item)

        return result


if len(sys.argv) > 2:
    filename = sys.argv[1]
    targetLang = sys.argv[2]
else:
    print("Specify source file and target language: python3 main.py Game/source.tsv Russian")
    exit(0)

basename = os.path.basename(filename)
targetDirectory = filename.replace(basename, "") + f"{targetLang}/"
source = get_source(filename)

intro = get_intro()
intro = intro.replace("%LANG%", targetLang)


def process(start_index, end_index, texts):
    targetPrompt = targetDirectory + basename.replace(".tsv", f"_{start_index}-{end_index}_prompt.txt")
    targetFilenameJson = targetDirectory + basename.replace(".tsv", f"_{start_index}-{end_index}.json")

    if not os.path.exists(targetDirectory):
        os.mkdir(targetDirectory)

    if not os.path.exists(targetFilenameJson):
        prompt = [{"role": "user", "content": intro}]
        jsonKeys = {"texts": texts}
        prompt.append({"role": "user", "content": json.dumps(jsonKeys)})

        with open(targetPrompt, "w") as f:
            json.dump(prompt, f)

        result = get_gpt_answer_from_array(prompt)
        if result is not None:
            with open(targetFilenameJson, "w") as f:
                f.write(result)

        res = json.loads(result)
        if res is None or 'texts' not in res:
            return
    else:
        with open(targetFilenameJson, 'r') as f:
            res = json.load(f)


index = 0
last_processed_index = 0
texts = []

print(f"Found {len(source)} lines in source file. Collecting API responses to {targetDirectory}...")
for line in source:
    index += 1
    if 'id' in line:
        texts.append({"id": line['id'], "text": line['English']})

    if index % 50 == 0:
        print(f"{index / len(source) * 100:.1f}% Process lines {last_processed_index}-{index}")
        process(last_processed_index, index, texts)
        last_processed_index = index + 1
        texts.clear()

if len(texts) > 0:
    process(last_processed_index, index, texts)

print(f"Result JSONs are generated in {targetDirectory}{basename}_*.json")
print(f"To generate new TSV file execute: python3 process.py {filename} {targetLang}")
