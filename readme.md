## OpenAI text translator

Генерирует tsv с переводами на разные языки.
Работает в два этапа:
1. Генерация json файлов с переводами на нужный язык
2. Замена нужных ключей/языков в tsv файле с переводами

### Использование:
1. Установи python3 + openai через pip3 
2. Скачай tsv с локализацией с Google Drive
3. Создай папку с именем игры, положите файл туда
4. !!! ВКЛЮЧИ VPN
5. Создай файл token.txt, укажи там токен для OpenAI и положи в ту же папку, где и скрипт generate.py
6. Запусти генерацию JSON по существующему файлу с переводами. Используй только эти названия языков: https://docs.unity3d.com/ScriptReference/SystemLanguage.html
```bash
python3 generate.py GameFolder/source.tsv French
```
При повторных запусках уже существующие JSON не будут запрашиваться из API, для экономии.
7. Запусти генерацию TSV по готовым JSON
```bash
python3 process.py GameFolder/source.tsv French
```
Скрипт заменит ключи в исходном файле и сохранит изменения в файле source_French.tsv.

Чтобы заменить следующий язык, можно использовать на вход скрипта source_French.tsv.