import sys
import glob
import csv
import json
import os

if len(sys.argv) > 2:
    filename = sys.argv[1]
    targetLang = sys.argv[2]
else:
    print("Specify source file and target language: python3 main.py Game/source.tsv Russian")
    exit(0)


def replace(filename, replaces):
    replacesCount = 0
    totalCount = 0
    result = []
    with open(filename, "r") as f:
        reader = csv.reader(f, delimiter="\t")
        langIndex = 0
        for line in reader:
            resultLine = line.copy()
            if langIndex == 0:
                for i in range(1, len(line)):
                    if line[i] == targetLang:
                        langIndex = i
                        print(f"Language {targetLang} found at column {langIndex}")
            else:
                for i in range(1, len(line)):
                    if i == langIndex:
                        totalCount += 1
                        if line[0] in replaces:
                            replacesCount += 1
                            resultLine[i] = replaces[line[0]]
            result.append(resultLine)

    return result, replacesCount, totalCount


basename = os.path.basename(filename)
targetDirectory = filename.replace(basename, "")
filenamesFilter = targetDirectory + f"{targetLang}/" + basename.replace(".tsv", f"*json")
filenameResult = targetDirectory + basename.replace(".tsv", f"_{targetLang}.tsv")
filenames = glob.glob(filenamesFilter)
print(f"Looking for JSONs at {filenamesFilter}")

keys = {}
for file in filenames:
    with open(file, "r") as f:
        j = json.load(f)
        for item in j["texts"]:
            if "id" in item:
                if len(item["id"]) > 0 and len(item["text"]) > 0:
                    if item["id"] in keys:
                        print(f"Already exists: {item['id']}")
                        continue

                    keys[item["id"]] = item["text"]

result, rc, tc = replace(filename, keys)
print(f"Found {tc} lines in source, found {len(keys)} keys in JSONs, replaced {rc} lines.")

with open(filenameResult, "w") as f:
    for line in result:
        f.write('\t'.join(line))
        f.write('\n')

print(f"Result saved to {filenameResult}.")